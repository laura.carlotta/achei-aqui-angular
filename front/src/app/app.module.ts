import { AppHeaderComponent } from './main-header/app-header-component';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AppLoginComponent } from './login-user/app-login-component';
import { FormsModule } from '@angular/forms';

@NgModule({
	declarations: [
		AppComponent,
		AppHeaderComponent,
		AppLoginComponent
	],
	imports: [
		BrowserModule,
		FormsModule
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule { }
